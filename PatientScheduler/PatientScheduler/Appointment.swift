//
//  Appointment.swift
//  PatientScheduler
//
//  Created by Tapepii Thirayu on 1/2/2563 BE.
//  Copyright © 2563 Tapepii Thirayu. All rights reserved.
//

import RealmSwift

class Appointment: Object {
    @objc dynamic var id: String = NSUUID().uuidString
    @objc dynamic var patientID = ""
    @objc dynamic var doctorID = ""
    @objc dynamic var date: Double = 0
    @objc dynamic var timingSlot = 0
    @objc dynamic var location = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(patientID: String, doctorID: String, date: Double, timingSlot: Int, location: String ) {
        self.init()
        self.patientID = patientID
        self.doctorID = doctorID
        self.date = date
        self.timingSlot = timingSlot
        self.location = location
    }
}

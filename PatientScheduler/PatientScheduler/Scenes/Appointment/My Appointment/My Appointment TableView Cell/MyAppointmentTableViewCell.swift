//
//  MyAppointmentTableViewCell.swift
//  PatientScheduler
//
//  Created by Rutchaneekorn Kawilo on 2/2/2563 BE.
//  Copyright © 2563 Tapepii Thirayu. All rights reserved.
//

import UIKit
import RealmSwift
import SDWebImage

protocol MyAppointmentTableViewCellDelegate {
    func myAppointmentTableViewCell(_ myAppointmentTableViewCell: MyAppointmentTableViewCell, didSelectCancelButton indexPath: IndexPath)
}

class MyAppointmentTableViewCell: UITableViewCell {
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var specialistLabel: UILabel!
    @IBOutlet weak var appointmentTimeDoctorInformationView: DoctorInformationView!
    @IBOutlet weak var cancelAppointmentButton: UIButton!
    
    let api = MockAPI()
    
    var indexPath: IndexPath?
    var delegate: MyAppointmentTableViewCellDelegate?
    
    var appointment: Appointment? {
        didSet {
            guard let appointment = appointment,
                let doctor = api.getDoctors(doctorID: appointment.doctorID) else {
                return
            }
            
            let imageURL = doctor.picture.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
            coverImageView.sd_setImage(with: URL(string: imageURL ?? ""),
                                              placeholderImage: UIImage(named: "user"),
                                              options: SDWebImageOptions.highPriority,
                                              completed: { image, error, cacheType, imageURL in })
            nameLabel.text = doctor.fname + " " + doctor.lname
            specialistLabel.text = doctor.department?.title
            
            let times = Constant.times
            appointmentTimeDoctorInformationView.text = "\(appointment.date.localDateFromTimeStamp()) at \(times[appointment.timingSlot])"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configurations()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func didSelectCancelAppointmentButton(_ sender: UIButton) {
        guard let indexPath = indexPath else {
            return
        }
        
        delegate?.myAppointmentTableViewCell(self, didSelectCancelButton: indexPath)
    }
    
    //MARK: - Configurations
    func configurations() {
        coverImageView.layer.cornerRadius = coverImageView.frame.height / 2
        coverImageView.layer.borderColor = UIColor.systemGroupedBackground.cgColor
        coverImageView.layer.borderWidth = 1
        
        appointmentTimeDoctorInformationView.icon = UIImage(named: "icon-calendar")
        
        cancelAppointmentButton.layer.cornerRadius = 4
    }
    
}

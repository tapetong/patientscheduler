//
//  MyAppointmentViewController.swift
//  PatientScheduler
//
//  Created by Rutchaneekorn Kawilo on 2/2/2563 BE.
//  Copyright © 2563 Tapepii Thirayu. All rights reserved.
//

import UIKit
import RealmSwift

class MyAppointmentViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    let api = MockAPI()
    
    var appointments : Results<Appointment>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurations()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getAppointments()
    }

}

// MARK: - Configurations
fileprivate extension MyAppointmentViewController {
    func configurations() {
        configureNavigationBar()
        configureTableView()
    }
    
    func configureNavigationBar() {
        setNavigationBarColor()
        title = "My Appointments"
    }
    
    func configureTableView() {
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        tableView.register(UINib(nibName: "MyAppointmentTableViewCell", bundle: nil), forCellReuseIdentifier: "MyAppointmentTableViewCell")
    }
}

//MARK: - Get Data
extension MyAppointmentViewController {
    func getAppointments() {
        appointments =  api.getMyAppointment(patientID: Constant.userID)
        tableView.reloadData()
    }
}

//MARK: - UITableViewDataSource
extension MyAppointmentViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appointments?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyAppointmentTableViewCell", for: indexPath) as! MyAppointmentTableViewCell
        cell.appointment = appointments?[indexPath.row]
        cell.indexPath = indexPath
        cell.delegate = self
        return cell
    }
}

//MARK: - UITableViewDelegate
extension MyAppointmentViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

//MARK: - MyAppointmentTableViewCellDelegate
extension MyAppointmentViewController: MyAppointmentTableViewCellDelegate {
    func myAppointmentTableViewCell(_ myAppointmentTableViewCell: MyAppointmentTableViewCell, didSelectCancelButton indexPath: IndexPath) {
        guard let appointmentID = appointments?[indexPath.row].id else { return }
        api.removeAppointment(appointmentID: appointmentID)
        getAppointments()
    }
}

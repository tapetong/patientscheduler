//
//  AvailableTimeCollectionViewCell.swift
//  PatientScheduler
//
//  Created by Rutchaneekorn Kawilo on 2/2/2563 BE.
//  Copyright © 2563 Tapepii Thirayu. All rights reserved.
//

import UIKit

class AvailableTimeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var backgroundTitleView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var time: String? {
        didSet {
            guard let time = time else {
                return
            }
            
            titleLabel.text = time
        }
    }
    
    var isBooked: Bool = false {
        didSet {
            if isBooked {
                titleLabel.textColor = UIColor.darkGray.withAlphaComponent(0.2)
            } else {
                titleLabel.textColor = UIColor.darkGray
            }
        }
    }
    
    var isChoosed: Bool = false {
        didSet {
            if isChoosed {
                backgroundTitleView.backgroundColor = UIColor.systemTeal
                titleLabel.textColor = UIColor.white
            } else {
                backgroundTitleView.backgroundColor = UIColor.white
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundTitleView.backgroundColor = UIColor.white
        backgroundTitleView.layer.cornerRadius = backgroundTitleView.frame.height / 2
        backgroundTitleView.layer.shadowColor = UIColor(white: 0, alpha: 0.1).cgColor
        backgroundTitleView.layer.shadowOffset = CGSize(width: 0, height: 1)
        backgroundTitleView.layer.shadowRadius = 6
        backgroundTitleView.layer.shadowOpacity = 1
    }

}

//
//  AvailableTimeView.swift
//  PatientScheduler
//
//  Created by Rutchaneekorn Kawilo on 2/2/2563 BE.
//  Copyright © 2563 Tapepii Thirayu. All rights reserved.
//

import UIKit
import RealmSwift

struct OpeningTime {
    var time: String?
    var isBooked: Bool = false
}

class AvailableTimeView: UIViewFromXIB {
    @IBOutlet weak var titlLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var timeSelectedIndexPath: IndexPath!
    var openingTimes: [OpeningTime] = []
    
    var appointments: Results<Appointment>? {
        didSet {
            guard let appointments = appointments else {
                openingTimes.removeAll()
                collectionView.reloadData()
                return
            }
            
            print("appointments.count \(appointments.count)")
            setOpeningTimes(appointments)
        }
    }
    
    //MARK: - Configurations
    override func configurations() {
        super.configurations()

        let cellSize = CGSize(width: 120 , height: 54)
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.itemSize = cellSize
        layout.minimumLineSpacing = 4
        layout.minimumInteritemSpacing = 0
        
        collectionView.setCollectionViewLayout(layout, animated: true)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "AvailableTimeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AvailableTimeCollectionViewCell")
    }
    
    fileprivate func setOpeningTimes(_ appointments: Results<Appointment>) {
        let times = Constant.times
        openingTimes.removeAll()
        
        for (indexTime, time) in times.enumerated() {
            var isBooked = false
            
            for indexAppointment in 0..<appointments.count {
                if appointments[indexAppointment].timingSlot == indexTime {
                    isBooked = true
                }
            }
            
            openingTimes.append(OpeningTime(time: time, isBooked: isBooked))
        }
        
        collectionView.reloadData()
    }

}

//MARK: - UICollectionViewDataSource, UICollectionViewDelegate
extension AvailableTimeView: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return openingTimes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AvailableTimeCollectionViewCell", for: indexPath) as! AvailableTimeCollectionViewCell
        cell.time = openingTimes[indexPath.row].time
        
        let isBooked = openingTimes[indexPath.row].isBooked
        cell.isBooked = isBooked
        
        if timeSelectedIndexPath != nil {
            cell.isChoosed = (indexPath == self.timeSelectedIndexPath)
        } else {
            cell.isChoosed = false
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if !openingTimes[indexPath.row].isBooked {
            self.timeSelectedIndexPath = indexPath
            collectionView.reloadData()
        }
    }
}

//
//  DoctorDetailHeaderView.swift
//  PatientScheduler
//
//  Created by Rutchaneekorn Kawilo on 1/2/2563 BE.
//  Copyright © 2563 Tapepii Thirayu. All rights reserved.
//

import UIKit
import RealmSwift
import SDWebImage

class DoctorDetailHeaderView: UIViewFromXIB {
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var specialistView: SpecialistView!
    
    var doctor: Doctor? {
        didSet {
            guard let doctor = doctor else {
                return
            }
            
            let imageURL = doctor.picture.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
            coverImageView.sd_setImage(with: URL(string: imageURL ?? ""),
                                              placeholderImage: UIImage(named: "user"),
                                              options: SDWebImageOptions.highPriority,
                                              completed: { image, error, cacheType, imageURL in })
            nameLabel.text = doctor.fname + " " + doctor.lname
            specialistView.text = doctor.department?.title
        }
    }
    
    override func configurations() {
        super.configurations()
        coverImageView.layer.cornerRadius = coverImageView.frame.height / 2
        coverImageView.layer.borderColor = UIColor.systemGroupedBackground.cgColor
        coverImageView.layer.borderWidth = 1
    }
}

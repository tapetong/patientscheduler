//
//  DoctorDetailViewController.swift
//  PatientScheduler
//
//  Created by Rutchaneekorn Kawilo on 1/2/2563 BE.
//  Copyright © 2563 Tapepii Thirayu. All rights reserved.
//

import UIKit
import FSCalendar
import RealmSwift

class DoctorDetailViewController: UIViewController {
    @IBOutlet weak var contentStackView: UIStackView!
    @IBOutlet weak var headerView: DoctorDetailHeaderView!
    @IBOutlet weak var calendarView: FSCalendar!
    @IBOutlet weak var calendarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var availableTimeView: AvailableTimeView!
    @IBOutlet weak var confirmButton: UIButton!
    
    let api = MockAPI()
    
    var currentDate: Double?
    var doctor: Doctor?
    var appointments: Results<Appointment>?

    override func viewDidLoad() {
        super.viewDidLoad()
        configurations()
        
        currentDate = Date().midnight.timeIntervalSince1970
        getAppointment()
    }
    
    @IBAction func didSelectConfirmButton(_ sender: UIButton) {
        createNewAppointment()
    }
}

//MARK: - Configurations
extension DoctorDetailViewController {
    func configurations() {
        configureNavigationBar()
        configureSectionsInStack()
    }
    
    func configureNavigationBar() {
        setNavigationBarColor()
        
        title = "Book an appointment"
        
        let closeButtonBar = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(didSelectCloseButton))
        self.navigationItem.rightBarButtonItem = closeButtonBar
    }

    func configureSectionsInStack() {
        headerView.doctor = doctor
        
        calendarView.select(Date())
        calendarView.scope = .week
        calendarView.dataSource = self
        calendarView.delegate = self
    }
}

//MARK: - Get Data
extension DoctorDetailViewController {
    func getAppointment() {
        guard let doctor = doctor,
              let currentDate = currentDate else {
                return
        }
        
        appointments =  api.getAppointment(date: currentDate, doctorID: doctor.id)
        availableTimeView.appointments = appointments
    }
    
    func createNewAppointment() {
        guard let doctor = doctor,
              let currentDate = currentDate,
              let timeSelectedIndexPath = availableTimeView.timeSelectedIndexPath else {
                return
        }
        
        let patientID = Constant.userID
        let timingSlot = timeSelectedIndexPath.row
        
        api.createNewAppointment(patientID: patientID, doctorID: doctor.id, date: currentDate, timingSlot: timingSlot, location: "Bangkok Hospital")
        modalAlertMessage(msgStr: "Appointment is successful") {
            self.dismiss(animated: true, completion: nil)
        }
    }
}

//MARK: - Actions
extension DoctorDetailViewController {
    @objc func didSelectCloseButton() {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: - FSCalendarDataSource, FSCalendarDelegate
extension DoctorDetailViewController: FSCalendarDataSource, FSCalendarDelegate {
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if date.midnight >= Date().midnight {
            currentDate = date.timeIntervalSince1970
            getAppointment()
        } else {
            availableTimeView.appointments = nil
        }
    }
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.calendarHeightConstraint.constant = bounds.height
        self.view.layoutIfNeeded()
    }
}

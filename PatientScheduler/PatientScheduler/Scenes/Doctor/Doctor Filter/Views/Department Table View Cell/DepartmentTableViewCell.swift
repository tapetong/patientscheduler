//
//  SpecializedMedicalGroupTableViewCell.swift
//  PatientScheduler
//
//  Created by Rutchaneekorn Kawilo on 1/2/2563 BE.
//  Copyright © 2563 Tapepii Thirayu. All rights reserved.
//

import UIKit
import RealmSwift
import SDWebImage

class DepartmentTableViewCell: UITableViewCell {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var department: Department? {
        didSet {
            guard let department = department else {
                return
            }
            
            let imageURL = department.icon.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
            iconImageView.sd_setImage(with: URL(string: imageURL ?? ""),
                                              placeholderImage: UIImage(named: "user"),
                                              options: SDWebImageOptions.highPriority,
                                              completed: { image, error, cacheType, imageURL in })
            titleLabel.text = department.title
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

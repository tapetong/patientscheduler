//
//  DoctorFilterViewController.swift
//  PatientScheduler
//
//  Created by Rutchaneekorn Kawilo on 1/2/2563 BE.
//  Copyright © 2563 Tapepii Thirayu. All rights reserved.
//

import UIKit
import RealmSwift

protocol DoctorFilterViewControllerDelegate {
    func doctorFilterViewController(_ doctorFilterViewController: DoctorFilterViewController, didSelectDepartment department: Department)
}

class DoctorFilterViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var departments: Results<Department>?
    var delegate: DoctorFilterViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurations()
        getDepartments()
    }
}

// MARK: - Configurations
fileprivate extension DoctorFilterViewController {
    func configurations() {
        configureNavigationBar()
        configureTableView()
    }
    
    func configureNavigationBar() {
        title = "Departments"
        let cancelButtonBar = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(didSelectCancelButton))
        self.navigationItem.rightBarButtonItem = cancelButtonBar
    }
    
    func configureTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        tableView.register(UINib(nibName: "DepartmentTableViewCell", bundle: nil), forCellReuseIdentifier: "DepartmentTableViewCell")
    }
}

// MARK: Get Data
extension DoctorFilterViewController {
    func getDepartments() {
        let api = MockAPI()
        departments = api.getDepartments()
        tableView.reloadData()
    }
}

//MARK: - Actions
extension DoctorFilterViewController {
    @objc func didSelectCancelButton() {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: - UITableViewDataSource
extension DoctorFilterViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return departments?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DepartmentTableViewCell", for: indexPath) as! DepartmentTableViewCell
        cell.department = departments?[indexPath.row]
        return cell
    }
}

//MARK: - UITableViewDelegate
extension DoctorFilterViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.doctorFilterViewController(self, didSelectDepartment: (departments?[indexPath.row])!)
        self.dismiss(animated: true, completion: nil)
    }
}

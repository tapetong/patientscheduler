//
//  DoctorInformationView.swift
//  PatientScheduler
//
//  Created by Rutchaneekorn Kawilo on 1/2/2563 BE.
//  Copyright © 2563 Tapepii Thirayu. All rights reserved.
//

import UIKit

class DoctorInformationView: UIViewFromXIB {
    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    
    var icon: UIImage? {
        didSet {
            guard let icon = icon else {
                return
            }
            
            iconImageView.image = icon.withRenderingMode(.alwaysTemplate)
            iconImageView.tintColor = UIColor(hexString: "919191")
        }
    }
    
    var text: String? {
        didSet {
            guard let text = text else {
                return
            }
            
            titleLabel.text = text
            titleLabel.textColor = UIColor(hexString: "919191")
        }
    }
}


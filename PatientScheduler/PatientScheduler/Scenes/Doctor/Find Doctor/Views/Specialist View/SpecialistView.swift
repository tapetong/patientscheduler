//
//  SpecialistView.swift
//  PatientScheduler
//
//  Created by Rutchaneekorn Kawilo on 1/2/2563 BE.
//  Copyright © 2563 Tapepii Thirayu. All rights reserved.
//

import UIKit

class SpecialistView: UIViewFromXIB {
    @IBOutlet weak var backgroundTitleView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var text: String? {
        didSet {
            guard let text = text else {
                return
            }
            
            titleLabel.text = text
        }
    }
    
    //MARK: - Configurations
    override func configurations() {
        super.configurations()
        
        backgroundTitleView.layer.cornerRadius = 4
    }
}

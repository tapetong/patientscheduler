//
//  DoctorTableViewCell.swift
//  PatientScheduler
//
//  Created by Rutchaneekorn Kawilo on 1/2/2563 BE.
//  Copyright © 2563 Tapepii Thirayu. All rights reserved.
//

import UIKit
import RealmSwift
import SDWebImage

class DoctorTableViewCell: UITableViewCell {
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var specialistView: SpecialistView!
    @IBOutlet weak var openingTimeDoctorInformationView: DoctorInformationView!
    
    var doctor: Doctor? {
        didSet {
            guard let doctor = doctor else {
                return
            }
            
            let imageURL = doctor.picture.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
            coverImageView.sd_setImage(with: URL(string: imageURL ?? ""),
                                              placeholderImage: UIImage(named: "user"),
                                              options: SDWebImageOptions.highPriority,
                                              completed: { image, error, cacheType, imageURL in })
            nameLabel.text = doctor.fname + " " + doctor.lname
            specialistView.text = doctor.department?.title
            openingTimeDoctorInformationView.text = "Open Timing: 9:00 am - 05.30 pm"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configurations()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - Configurations
    func configurations() {
        coverImageView.layer.cornerRadius = coverImageView.frame.height / 2
        coverImageView.layer.borderColor = UIColor.systemGroupedBackground.cgColor
        coverImageView.layer.borderWidth = 1
        
        openingTimeDoctorInformationView.icon = UIImage(named: "icon-time")
    }
}

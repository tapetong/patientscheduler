//
//  FindDoctorViewController.swift
//  PatientScheduler
//
//  Created by Rutchaneekorn Kawilo on 1/2/2563 BE.
//  Copyright (c) 2563 Tapepii Thirayu. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit
import RealmSwift

protocol FindDoctorDisplayLogic: class {
    func displayDoctors(viewModel: FindDoctor.GetDoctor.ViewModel)
    func displayErrorNotification(errorMsg: String)
}

class FindDoctorViewController: UIViewController, FindDoctorDisplayLogic {
    @IBOutlet weak var tableView: UITableView!
    
    lazy var searchBar: UISearchBar = UISearchBar()
    var doctors: Results<Doctor>?
    
    var interactor: FindDoctorBusinessLogic?
    var router: (NSObjectProtocol & FindDoctorRoutingLogic & FindDoctorDataPassing)?
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
  
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() {
        let viewController = self
        let interactor = FindDoctorInteractor()
        let presenter = FindDoctorPresenter()
        let router = FindDoctorRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configurations()
        getDoctors(searchText: nil)
    }
}

// MARK: - Configurations
fileprivate extension FindDoctorViewController {
    func configurations() {
        configureNavigationBar()
        configureTableView()
        hideKeyboardWhenTappedAround()
    }
    
    func configureNavigationBar() {
        setNavigationBarColor()
        searchBar.frame = .zero
        searchBar.placeholder = "Search Doctors"
        searchBar.delegate = self
        
        let filterIconImage = UIImage(named: "icon-filter")
        let filterButtonBar = UIBarButtonItem(image: filterIconImage, style: .plain, target: self, action: #selector(didSelectFilterButton))
        
        self.navigationItem.titleView = searchBar
        self.navigationItem.rightBarButtonItem = filterButtonBar
    }
    
    func configureTableView() {
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        tableView.register(UINib(nibName: "DoctorTableViewCell", bundle: nil), forCellReuseIdentifier: "DoctorTableViewCell")
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
}

//MARK: - Actions
extension FindDoctorViewController {
    @objc func didSelectFilterButton() {
        router?.routeToDoctorFilter()
    }
    
    @objc func dismissKeyboard() {
        searchBar.endEditing(true)
    }
}

// MARK: Request, Display Data
extension FindDoctorViewController {
    func getDoctors(searchText: String?) {
        let request = FindDoctor.GetDoctor.Request(searchText: searchText)
        interactor?.getDoctors(request: request)
    }
    
    func displayDoctors(viewModel: FindDoctor.GetDoctor.ViewModel) {
        doctors = viewModel.doctors!
        tableView.reloadData()
    }

    func displayErrorNotification(errorMsg: String) {

    }
}

//MARK: - UITableViewDataSource
extension FindDoctorViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return doctors?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DoctorTableViewCell", for: indexPath) as! DoctorTableViewCell
        cell.doctor = doctors?[indexPath.row]
        return cell
    }
}

//MARK: - UITableViewDelegate
extension FindDoctorViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let doctor = (doctors?[indexPath.row])!
        router?.routeToDoctorDetail(doctor: doctor)
    }
}

//MARK: - UISearchBarDelegate
extension FindDoctorViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        getDoctors(searchText: searchText)
    }
}

//MARK: - DoctorFilterViewControllerDelegate
extension FindDoctorViewController: DoctorFilterViewControllerDelegate {
    func doctorFilterViewController(_ doctorFilterViewController: DoctorFilterViewController, didSelectDepartment department: Department) {
        searchBar.text = department.title
        getDoctors(searchText: department.title)
    }
}

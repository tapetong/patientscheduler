//
//  ViewController.swift
//  PatientScheduler
//
//  Created by Tapepii Thirayu on 1/2/2563 BE.
//  Copyright © 2563 Tapepii Thirayu. All rights reserved.
//

import UIKit
import RealmSwift

class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        //print(documentsPath)
        
        let api = MockAPI()

        if let user = api.logIn(email: "test01@gmail.com", password: "12345678") {
            print(user)
        } else {
            print("User Not Found")
        }
        
    }
}


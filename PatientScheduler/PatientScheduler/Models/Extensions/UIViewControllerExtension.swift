//
//  UIViewControllerExtension.swift
//  PatientScheduler
//
//  Created by Rutchaneekorn Kawilo on 1/2/2563 BE.
//  Copyright © 2563 Tapepii Thirayu. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func modalAlertMessage(msgStr: String, completion: (() -> ())? = nil) {
        let alertVC = UIAlertController.init(title: msgStr, message: nil, preferredStyle: .alert)
        let okAction = UIAlertAction.init(title: "ตกลง", style: .default) { (_) in
            completion?()
        }
        
        alertVC.addAction(okAction)
        self.present(alertVC, animated: true, completion: nil)
    }
    
    func setNavigationBarColor() {
        if #available(iOS 13.0, *) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.backgroundColor = .white
            navBarAppearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
            self.navigationController?.navigationBar.standardAppearance = navBarAppearance
            self.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
        }
        
        self.navigationController?.navigationBar.barTintColor = .black
        self.navigationController?.navigationBar.tintColor = UIColor.systemTeal
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        self.navigationController?.navigationBar.isTranslucent = false
        
        UIApplication.shared.statusBarStyle = .default
    }
}

//
//  StackViewExtension.swift
//  Tosh
//
//  Created by Tapepii Thirayu on 30/11/2562 BE.
//  Copyright © 2562 Tapepii Thirayu. All rights reserved.
//

import UIKit

extension UIStackView {
    func addArrangedSubviews(views: [UIView]) {
        for view in views {
            self.addArrangedSubview(view)
        }
    }

    func removeAllArrangedSubviews() {
        for view in self.arrangedSubviews {
            self.removeArrangedSubview(view)
            view.removeFromSuperview()
        }
    }
}

//
//  DoubleExtension.swift
//  Tosh
//
//  Created by Rutchaneekorn Kawilo on 4/12/2562 BE.
//  Copyright © 2562 Tapepii Thirayu. All rights reserved.
//

import UIKit

extension Double {
    func putComma(decimal: Int) -> String {
        let largeNumber = self
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        numberFormatter.minimumFractionDigits = 0
        numberFormatter.maximumFractionDigits = decimal
        return numberFormatter.string(from: NSNumber(value: largeNumber))!
    }

    func removeComma() -> String {
        let str: String = String(format:"%f", self)
        return str.replacingOccurrences(of: ",", with: "", options: .literal, range: nil)
    }

    func localDateFromTimeStamp() -> String {
        let time = self
        let date = NSDate(timeIntervalSince1970:time)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yy"
        dateFormatter.timeZone = TimeZone.current
        //dateFormatter.locale = Locale(identifier: "th") 
        let localDate = dateFormatter.string(from: date as Date)
        return localDate
    }
    
    func localDateTimeFromTimeStamp() -> String {
        let time = self
        let date = NSDate(timeIntervalSince1970:time)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "วันที่ dd/MM/YYYY เวลา HH:mm"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale(identifier: "th")
        let localDate = dateFormatter.string(from: date as Date)
        return localDate
    }
    
    func localTimeFromTimeStamp() -> String {
        let time = self/1000
        let date = NSDate(timeIntervalSince1970:time)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale(identifier: "th")
        let localDate = dateFormatter.string(from: date as Date)
        return localDate
    }

    func milSecondsToHoursMinutesNumber () -> String {
        let totalSeconds = Int(self) / 1000

        let hours = Int(totalSeconds / 3600)
        let minutes = Int( ( totalSeconds - (hours * 3600) ) /  60 )
        let seconds = totalSeconds - (hours * 3600) - (minutes * 60)

        var timeStr = ""

        timeStr += String(format:"%02d:", hours)
        timeStr += String(format:"%02d:", max(0, minutes))
        timeStr += String(format:"%02d", max(0, seconds))

        return timeStr
    }
}

//
//  Date.swift
//  PatientScheduler
//
//  Created by Rutchaneekorn Kawilo on 2/2/2563 BE.
//  Copyright © 2563 Tapepii Thirayu. All rights reserved.
//

import Foundation
import UIKit

extension Date {
    var midnight: Date{
        let cal = Calendar(identifier: .gregorian)
        return cal.startOfDay(for: self)
    }
}

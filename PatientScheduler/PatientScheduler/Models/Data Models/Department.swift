//
//  Department.swift
//  PatientScheduler
//
//  Created by Tapepii Thirayu on 1/2/2563 BE.
//  Copyright © 2563 Tapepii Thirayu. All rights reserved.
//

import RealmSwift

class Department: Object {
    @objc dynamic var id = ""
    @objc dynamic var title = ""
    @objc dynamic var icon = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

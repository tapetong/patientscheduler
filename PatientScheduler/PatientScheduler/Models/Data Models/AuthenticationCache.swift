//
//  authenCache.swift
//  PatientScheduler
//
//  Created by Tapepii Thirayu on 1/2/2563 BE.
//  Copyright © 2563 Tapepii Thirayu. All rights reserved.
//

import RealmSwift

class AuthenticationCache: Object {
    @objc dynamic var email = ""
    @objc dynamic var password = ""
}


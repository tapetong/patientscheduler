//
//  Doctor.swift
//  PatientScheduler
//
//  Created by Tapepii Thirayu on 1/2/2563 BE.
//  Copyright © 2563 Tapepii Thirayu. All rights reserved.
//

import RealmSwift

class Doctor: Object {
    @objc dynamic var id = ""
    @objc dynamic var title = ""
    @objc dynamic var fname = ""
    @objc dynamic var lname = ""
    @objc dynamic var picture = ""
    @objc dynamic var department: Department?
    @objc dynamic var lattitude = ""
    @objc dynamic var longtitude = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

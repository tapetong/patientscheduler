//
//  UserProfile.swift
//  PatientScheduler
//
//  Created by Tapepii Thirayu on 1/2/2563 BE.
//  Copyright © 2563 Tapepii Thirayu. All rights reserved.
//

import RealmSwift

class UserProfile: Object {
    @objc dynamic var email = ""
    @objc dynamic var password = ""
    @objc dynamic var fname = ""
    @objc dynamic var lname = ""
    @objc dynamic var tel = ""
    @objc dynamic var picture = ""
    
    override static func primaryKey() -> String? {
        return "email"
    }
}

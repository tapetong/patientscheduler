//
//  AppDelegate.swift
//  PatientScheduler
//
//  Created by Tapepii Thirayu on 1/2/2563 BE.
//  Copyright © 2563 Tapepii Thirayu. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        configurations()
        return true
    }
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
       
    }
    
    func configurations() {
        IQKeyboardManager.shared.enable = true
        setDefaultRealmForUser()
    }
    
    func setDefaultRealmForUser() {
        let defaultRealmPath = Realm.Configuration.defaultConfiguration.fileURL!
        let bundleReamPath = Bundle.main.path(forResource: "default_appointment", ofType:"realm")

        if !FileManager.default.fileExists(atPath: defaultRealmPath.path) {
            do
            {
                try FileManager.default.copyItem(atPath: bundleReamPath!, toPath: defaultRealmPath.path)
            }
            catch let error as NSError {
                // Catch fires here, with an NSError being thrown
                print("error occurred, here are the details:\n \(error)")
            }
        }
    }
}


//
//  Constants.swift
//  PatientScheduler
//
//  Created by Rutchaneekorn Kawilo on 2/2/2563 BE.
//  Copyright © 2563 Tapepii Thirayu. All rights reserved.
//

import Foundation

enum Constant {
    static let times: [String] = ["9:00 AM", "9:30 AM", "10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM", "01:00 PM", "01:30 PM", "02:00 PM", "02:30 PM", "03:00 PM", "03:30 PM", "04:00 PM", "04:30 PM", "05:00 PM", "05:30 PM"]
    static let userID: String = "1"
}

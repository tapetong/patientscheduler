//
//  MockAPI.swift
//  PatientScheduler
//
//  Created by Tapepii Thirayu on 1/2/2563 BE.
//  Copyright © 2563 Tapepii Thirayu. All rights reserved.
//

import Foundation
import RealmSwift

class MockAPI {
    func tryToAutoLogInUsingCookie() -> UserProfile? {
        let realm = try! Realm()
        let users = realm.objects(AuthenticationCache.self)
        guard let email = users.first?.email, let password = users.first?.password else { return nil }
        return logIn(email: email, password: password)
    }
    
    func logIn(email: String, password: String) -> UserProfile? {
        let realm = try! Realm()
        let predicate = NSPredicate(format: "email = %@ AND password = %@", email, password)
        let users = realm.objects(UserProfile.self).filter(predicate)
        let user = (users.count > 0) ? users.first : nil
        return user
    }
    
    func register(email: String, password: String, tel: String) -> UserProfile? {
        // Check Exist user
        if getUserFromEmail(email: email) == nil {
            return nil
        }
        
        //Create New User
        let realm = try! Realm()
        let newUser = UserProfile()
        newUser.email = email
        newUser.password = password
        newUser.tel = tel
        
        //Add User to database
        try! realm.write {
            realm.add(newUser)
        }
        
        return newUser
    }
    
    func getUserFromEmail(email: String) -> UserProfile? {
        let realm = try! Realm()
        let predicate = NSPredicate(format: "email = %@", email)
        let users = realm.objects(UserProfile.self).filter(predicate)
        let user = (users.count > 0) ? users.first : nil
        return user
    }
    
    func getDepartments() -> Results<Department> {
        let realm = try! Realm()
        let departments = realm.objects(Department.self)
        return departments
    }
    
    func getDoctors(searchText: String?) -> Results<Doctor> {
        let realm = try! Realm()
        
        if let keyword = searchText, !keyword.isEmpty {
            let predicate = NSPredicate(format: "department.title CONTAINS[c] %@ OR fname CONTAINS[c] %@ OR lname CONTAINS[c] %@", keyword, keyword, keyword)
            return realm.objects(Doctor.self).filter(predicate)
        } else {
            return realm.objects(Doctor.self)
        }
    }
    
    func getDoctors(doctorID: String?) -> Doctor? {
        let realm = try! Realm()
        
        if let doctorID = doctorID {
            let predicate = NSPredicate(format: "id = %@", doctorID)
            return realm.objects(Doctor.self).filter(predicate).first
        }
        
        return nil
    }
    
    func createNewAppointment(patientID: String, doctorID: String, date: Double, timingSlot: Int, location: String) {
        let realm = try! Realm()
        let newAppointment = Appointment(patientID: patientID,
                                   doctorID: doctorID,
                                   date: date,
                                   timingSlot: timingSlot,
                                   location: location)
        
        try! realm.write {
            realm.add(newAppointment)
        }
    }
    
    func removeAppointment(appointmentID: String) {
        let realm = try! Realm()
        
        let predicate = NSPredicate(format: "id = %@", appointmentID)
        let appointment = realm.objects(Appointment.self).filter(predicate)
        
        try! realm.write {
            realm.delete(appointment)
        }
    }
    
    func getAppointments() -> Results<Appointment> {
        let realm = try! Realm()
        return realm.objects(Appointment.self)
    }
    
    func getAppointment(date: Double, doctorID: String) -> Results<Appointment> {
        let realm = try! Realm()
        
        let predicate = NSPredicate(format: "date = %lf AND doctorID = %@", date, doctorID)
        let appointments = realm.objects(Appointment.self).filter(predicate)
        
        return appointments
    }
    
    func getMyAppointment(patientID: String) -> Results<Appointment> {
        let realm = try! Realm()
        let currentDate = Date().timeIntervalSince1970
        let predicate = NSPredicate(format: "patientID = %@ AND date > %lf", patientID, currentDate)
        return realm.objects(Appointment.self).filter(predicate).sorted(byKeyPath:"date", ascending: true)
    }
}

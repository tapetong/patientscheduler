//
//  PatientSchedulerTests.swift
//  PatientSchedulerTests
//
//  Created by Tapepii Thirayu on 1/2/2563 BE.
//  Copyright © 2563 Tapepii Thirayu. All rights reserved.
//
import Foundation
import XCTest

@testable import PatientScheduler

class PatientSchedulerTests: XCTestCase {
    var api: MockAPI!
    
    override func setUp() {
        api = MockAPI()
    }

    override func tearDown() {
        api = nil
    }

    func testGetDoctors() {
        let doctors = api.getDoctors(searchText: nil)
        XCTAssertTrue(doctors.count > 0)
    }
    
    func testGetDepartmentDoctors() {
        let departments = api.getDepartments()
        XCTAssertTrue(departments.count > 0)
    }
    
    func testGetAppointment() {
        let appointment = api.getAppointments()
        XCTAssertTrue(appointment.count == 0)
    }
    
    func testGetDoctor() {
        let doctors = api.getDoctors(searchText: "Herm")
        XCTAssertTrue(doctors.count == 1)
    }
    
    func testCreateAndRemoveAppointment() {
        //Before Create
        var appointments = api.getAppointments()
        XCTAssertTrue(appointments.count == 0)
        
        //Created
        let date = Date().timeIntervalSince1970
        api.createNewAppointment(patientID: "1", doctorID: "1", date: date, timingSlot: 1, location: "Bangkok")
        appointments = api.getAppointments()
        XCTAssertTrue(appointments.count == 1)
        
        //After remove
        api.removeAppointment(appointmentID: appointments.first!.id)
        XCTAssertTrue(appointments.count == 0)
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
}
